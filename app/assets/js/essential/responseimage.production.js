/**
 * jQuery Picture
 * http://jquerypicture.com
 * http://github.com/Abban/jQuery-Picture
 * 
 * May 2012
 * 
 * @version 0.9
 * @author Abban Dunne http://abandon.ie
 * @license MIT
 * 
 * jQuery Picture is a plugin to add support for responsive images to your layouts.
 * It supports both figure elements with some custom data attributes and the new
 * proposed picture format. This plugin will be made redundant when the format is
 * approved and implemented by browsers. Lets hope that happens soon. In the meantime
 * this plugin will be kept up to date with latest developments.
 * 
 */(function(e){e.fn.picture=function(t){var n={container:null},r=e.extend({},n,t);this.each(function(){function t(t){if(t)if(a.get(0).tagName.toLowerCase()=="figure"){var f=a.data();e.each(f,function(e){var t;t=e.replace(/[^\d.]/g,"");t&&s.push(t)})}else a.find("source").each(function(){var t,n;t=e(this).attr("media");if(t){n=t.replace(/[^\d.]/g,"");s.push(n)}});var c=0;r.container==null?o=e(window).width()*l:o=e(r.container).width()*l;e.each(s,function(e,t){parseInt(o)>=parseInt(t)&&parseInt(c)<=parseInt(t)&&(c=t)});if(u!==c){u=c;a.get(0).tagName.toLowerCase()=="figure"?i():n()}}function n(){var t=new Object;a.find("source").each(function(){var n,r,i;n=e(this).attr("media");r=e(this).attr("src");n?i=n.replace(/[^\d.]/g,""):i=0;t[i]=r});if(a.find("img").length==0){var n='<img src="'+t[u]+'" style="'+a.attr("style")+'" alt="'+a.attr("alt")+'">';a.find("a").length==0?a.append(n):a.find("a").append(n)}else a.find("img").attr("src",t[u])}function i(){var t=new Object,n=a.data();e.each(n,function(e,n){var r;r=e.replace(/[^\d.]/g,"");r||(r=0);t[r]=n});if(a.find("img").length==0){var r='<img src="'+t[u]+'" alt="'+a.attr("title")+'">';a.find("a").length==0?a.prepend(r):a.find("a").prepend(r)}else a.find("img").attr("src",t[u])}var s=new Array,o,u,a,f,l=1;window.devicePixelRatio!==undefined&&(l=window.devicePixelRatio);a=e(this);t(!0);f=!1;e(window).resize(function(){f!==!1&&clearTimeout(f);f=setTimeout(t,200)})})}})(jQuery);