# Require any additional compass plugins here.
project_type = :rails

module Sass::Script::Functions

  module CustomSassExtensions
    def compact(*args)
      sep = :comma
      if args.size == 1 && args.first.is_a?(Sass::Script::List)
        args = args.first.value
        sep = args.first.separator
      end
      Sass::Script::List.new(args.reject{|a| !a.to_bool}, sep)
    end
  end

  include CustomSassExtensions

end

additional_import_paths = ["app/assets/sass"]
